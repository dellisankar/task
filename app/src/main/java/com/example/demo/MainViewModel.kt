package com.example.demo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {

    private val _allFieldsValid = MutableLiveData(false)
    val isAllFieldValid: LiveData<Boolean>
        get() = _allFieldsValid

    private val _submitted = MutableLiveData(false)
    val isSubmitted: LiveData<Boolean>
        get() = _submitted

    private var _panValid = false
    private var _dayValid = false
    private var _monthValid = false
    private var _yearValid = false

    fun panValid(state: Boolean){
        _panValid = state
        updateValidation()
    }

    fun dayValid(state: Boolean){
        _dayValid = state
        updateValidation()
    }

    fun monthValid(state: Boolean){
        _monthValid = state
        updateValidation()
    }

    fun yearValid(state: Boolean){
        _yearValid = state
        updateValidation()
    }

    fun submitDetails(){
        viewModelScope.launch {
            delay(2000)
            _submitted.value = true
        }
    }

    private fun updateValidation(){
        _allFieldsValid.value = _panValid && _dayValid && _monthValid && _yearValid
    }
}