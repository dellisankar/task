package com.example.demo

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.demo.MainActivity.Companion.DAY_LENGTH
import com.example.demo.MainActivity.Companion.MONTH_LENGTH
import com.example.demo.MainActivity.Companion.PAN_LENGTH
import com.example.demo.MainActivity.Companion.YEAR_LENGTH
import com.example.demo.databinding.ActivityMainBinding
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    private val calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.vm = viewModel
        binding.lifecycleOwner = this

        binding.apply {
            dontHavePan.setOnClickListener {
                finish()
            }
            panField.addTextChangedListener(FieldValidator(Fields.PAN, viewModel, calendar))
            dayField.addTextChangedListener(FieldValidator(Fields.DAY, viewModel, calendar))
            monthField.addTextChangedListener(FieldValidator(Fields.MONTH, viewModel, calendar))
            yearField.addTextChangedListener(FieldValidator(Fields.YEAR, viewModel, calendar))
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.isAllFieldValid.observe(this, {
            binding.nextBtn.isEnabled = it
        })
        viewModel.isSubmitted.observe(this, {
            if (it){
                showToast("details submitted successfully")
                finish()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.apply {
            panField.removeTextChangedListener(FieldValidator(Fields.PAN, viewModel, calendar))
            dayField.removeTextChangedListener(FieldValidator(Fields.DAY, viewModel, calendar))
            monthField.removeTextChangedListener(FieldValidator(Fields.MONTH, viewModel, calendar))
            yearField.removeTextChangedListener(FieldValidator(Fields.YEAR, viewModel, calendar))
        }
    }

    companion object {
        const val PAN_LENGTH = 10
        const val DAY_LENGTH = 2
        const val MONTH_LENGTH = 2
        const val YEAR_LENGTH = 4
    }
}

class FieldValidator(private val field: Fields,
                     private val viewModel: MainViewModel,
                     calendar: Calendar) : TextWatcher {

    private val minLength = when (field) {
        Fields.PAN -> PAN_LENGTH
        Fields.DAY -> DAY_LENGTH
        Fields.MONTH -> MONTH_LENGTH
        Fields.YEAR -> YEAR_LENGTH
    }
    private var isValidField: Boolean = false
    private val yyyy = calendar.get(Calendar.YEAR)

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        Log.d("Testing", "before text:-- $p0")
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        val input: String = p0.toString().trim()
        if (!TextUtils.isEmpty(input)) {
            when(field) {
                Fields.PAN -> {
                    isValidField = input.length == minLength
                    viewModel.panValid(isValidField)
                }
                Fields.DAY -> {
                    val day: Int = input.toInt()
                    isValidField= input.length == minLength && day <= 31 && day != 0
                    viewModel.dayValid(isValidField)
                }
                Fields.MONTH -> {
                    val month: Int = input.toInt()
                    isValidField = input.length == minLength && month <= 12 && month != 0
                    viewModel.monthValid(isValidField)
                }
                Fields.YEAR -> {
                    val year: Int = input.toInt()
                    isValidField = input.length == minLength && year <= yyyy && year != 0
                    viewModel.yearValid(isValidField)
                }
            }
        }
    }

    override fun afterTextChanged(p0: Editable?) {
        Log.d("Testing", "after text:-- $p0")
    }
}

enum class Fields {
    PAN,
    DAY,
    MONTH,
    YEAR
}